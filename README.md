# Wedoogift Frontend challenge - Nathan Couton

## App

The app is available at localhost:4200 which defaults to localhost:4200/shop/5.

### Routing

I built basic routing to manage different shops.

## Component

The main component is `CalculatorComponent`, which implements `ControlValueAccessor` to make it usable as a Form Control. I removed the `@Output()` because it made more sens to subscribe the `valueChanges` to get the components value whenever it changes.

The `ShopComponent` puts it in a form for testing purpose.

## What's done

1. if the desired amount is possible, **display a list of the cards needed to reach that amount**
2. if the desired amount is not possible, then the component displays `Montant précédent` and `Montant suivant` buttons to allow the user to choose another possible amount. The possible amounts are displayed on hovering the buttons for more clarity.
3. if the desired amount is higher or lower than the possible amounts, the component will auto-correct the user with min or max possible value
4. The component has it's own internal state management:
   1. The `Valider` button will be enabled only if the input has a value. If the input was already submitted, the input will become accessible again when the input changes.
   2. A message is desplayed above the resulting cards depending on the situation (amount existing, not existing, too low/too high).
5. The component is usable in a Reactive Form.

The `value` of the component is of type:

```typescript
interface CalculatorComponentValue {
  value: number;
  cards: number[];
}
```

### What could be done

- Improving accessibility
- Improve responsiveness

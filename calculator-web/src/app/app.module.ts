import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { AmountInputComponent } from './components/amount-input/amount-input.component';
import { CalculatorSectionComponent } from './components/calculator-section/calculator-section.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ResultComponent } from './components/result/result.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, InitialNavigation } from '@angular/router';
import { ShopComponent } from './components/shop/shop.component';
import { routes } from './routes';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    AmountInputComponent,
    CalculatorSectionComponent,
    CalculatorComponent,
    ResultComponent,
    ShopComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

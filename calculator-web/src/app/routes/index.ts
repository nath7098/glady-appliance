import { NotFoundComponent } from '../components/not-found/not-found.component';
import { ShopComponent } from '../components/shop/shop.component';
import { Routes } from '@angular/router';

export const routes = [
  {
    path: '',
    redirectTo: 'shop/5',
    pathMatch: 'full',
  },
  {
    path: 'shop/:id',
    component: ShopComponent,
  },
  {
    path: '404',
    component: NotFoundComponent,
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full',
  },
] as Routes;

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopComponent } from './shop.component';
import { CalculatorComponent } from '../calculator/calculator.component';
import { CalculatorSectionComponent } from '../calculator-section/calculator-section.component';
import { AmountInputComponent } from '../amount-input/amount-input.component';
import { ResultComponent } from '../result/result.component';
import { CardComponent } from '../card/card.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('ShopComponent', () => {
  let fixture: ComponentFixture<ShopComponent>;
  let component: ShopComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ShopComponent,
        CalculatorComponent,
        CalculatorSectionComponent,
        AmountInputComponent,
        ResultComponent,
        CardComponent,
      ],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(ShopComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the form with the calculator control', () => {
    fixture.detectChanges();
    expect(component.form.get('calculator')).toBeDefined();
  });

  it('should enable the calculator on init', () => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();

      expect(component.form.controls['calculator'].enabled).toBe(true);
    });
  });

  it('should enable the calculator when form is enabled', () => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      component.form.enable();
      fixture.detectChanges();

      expect(component.form.controls['calculator'].enabled).toBe(true);
    });
  });

  it('should disable the calculator when form is disabled', () => {
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      component.form.disable();
      fixture.detectChanges();

      expect(component.form.controls['calculator'].disabled).toBe(true);
    });
  });
});

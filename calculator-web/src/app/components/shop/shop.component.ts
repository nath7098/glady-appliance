import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Shop } from 'src/app/models/shop.model';
import { findShop } from 'src/app/utils/mocks/shops';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  shop: Shop = {};

  form: FormGroup = {} as FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe((params) => {
      const id: number = Number.parseInt(params['id']);
      const shopSearched = findShop(id);
      if (shopSearched) {
        this.shop = shopSearched;
      } else {
        this.router.navigate(['404']);
      }
    });
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      calculator: { value: {}, disabled: false },
    });

    this.form
      .get('calculator')
      ?.valueChanges.subscribe((value) => this.test(value));
  }

  test(e: any) {
    // might do something
  }
}

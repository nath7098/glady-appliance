import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CalculatorComponentValue } from 'src/app/models/calculator-component-value.model';

@Component({
  selector: 'app-amount-input',
  templateUrl: './amount-input.component.html',
  styleUrls: ['./amount-input.component.scss'],
})
export class AmountInputComponent {
  @Input() desiredAmount?: number;
  @Input() previousSuggestion?: CalculatorComponentValue;
  @Input() nextSuggestion?: CalculatorComponentValue;
  @Input() disabled: boolean = false;
  @Output() desiredAmountChange = new EventEmitter<number>();
  @Output() enter = new EventEmitter<number>();
  @Output() suggestionSelect = new EventEmitter<CalculatorComponentValue>();

  setDesiredAmount($event: any) {
    this.desiredAmount = $event.target.value;
    this.desiredAmountChange.emit(this.desiredAmount);
  }

  onEnter() {
    this.enter.emit(this.desiredAmount);
  }

  setSuggestionSelected($event: any, suggestion: CalculatorComponentValue) {
    // prevents emitting when enter from the input a second time
    if ($event && $event.detail == 0) return;
    this.suggestionSelect.emit(suggestion);
  }
}

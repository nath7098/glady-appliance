import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmountInputComponent } from './amount-input.component';
import { By } from '@angular/platform-browser';

describe('AmountInputComponent', () => {
  let component: AmountInputComponent;
  let fixture: ComponentFixture<AmountInputComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AmountInputComponent],
    });

    fixture = TestBed.createComponent(AmountInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render input with label and buttons', () => {
    component.desiredAmount = 100;
    component.previousSuggestion = { value: 50, cards: [1, 2, 3] };
    component.nextSuggestion = { value: 150, cards: [4, 5, 6] };
    fixture.detectChanges();

    const inputElement = fixture.debugElement.query(By.css('input'));
    const labelElement = fixture.debugElement.query(By.css('label'));
    const prevButton = fixture.debugElement.query(
      By.css('.amount__input--prev')
    );
    const nextButton = fixture.debugElement.query(
      By.css('.amount__input--next')
    );

    expect(inputElement).toBeTruthy();
    expect(labelElement.nativeElement.textContent).toContain('Montant désiré');
    expect(prevButton.nativeElement.textContent).toContain('Montant précédent');
    expect(nextButton.nativeElement.textContent).toContain('Montant suivant');
  });

  it('should emit desiredAmountChange event on input change', () => {
    const desiredAmountSpy = spyOn(component.desiredAmountChange, 'emit');
    const inputElement = fixture.debugElement.query(By.css('input'));

    inputElement.triggerEventHandler('input', { target: { value: 100 } });
    fixture.detectChanges();

    expect(desiredAmountSpy).toHaveBeenCalledWith(100);
  });

  it('should emit enter event on input Enter key press', () => {
    const enterSpy = spyOn(component.enter, 'emit');
    const inputElement = fixture.debugElement.query(By.css('input'));

    inputElement.triggerEventHandler('keyup.enter', {});
    fixture.detectChanges();

    expect(enterSpy).toHaveBeenCalledWith(component.desiredAmount);
  });

  it('should emit suggestionSelect event on suggestion button click', () => {
    const suggestionSelectSpy = spyOn(component.suggestionSelect, 'emit');
    component.nextSuggestion = { value: 150, cards: [4, 5, 6] };
    fixture.detectChanges();

    const nextButton = fixture.debugElement.query(
      By.css('.amount__input--next')
    );
    nextButton.triggerEventHandler('click', {});
    fixture.detectChanges();

    expect(suggestionSelectSpy).toHaveBeenCalledWith(component.nextSuggestion);
  });

  it('should not emit suggestionSelect event on suggestion button click if enter key press', () => {
    const suggestionSelectSpy = spyOn(component.suggestionSelect, 'emit');
    component.nextSuggestion = { value: 150, cards: [4, 5, 6] };
    fixture.detectChanges();

    const nextButton = fixture.debugElement.query(
      By.css('.amount__input--next')
    );
    nextButton.triggerEventHandler('click', { detail: 0 }); // Simulating Enter key press
    fixture.detectChanges();

    expect(suggestionSelectSpy).not.toHaveBeenCalled();
  });
});

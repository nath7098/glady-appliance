import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CardComponent } from './card.component';
import { By } from '@angular/platform-browser';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent],
    });

    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render card value', () => {
    component.value = 20;
    fixture.detectChanges();

    const cardValue = fixture.debugElement.query(
      By.css('.card__value')
    ).nativeElement;
    expect(cardValue.textContent).toContain('20');
  });

  it('should not render card value if value is not provided', () => {
    component.value = undefined;
    fixture.detectChanges();

    const cardValue = fixture.debugElement.query(By.css('.card__value'));
    expect(cardValue).toBeNull();
  });

  it('should display correct card value', () => {
    component.value = 35;
    fixture.detectChanges();

    const cardValue = fixture.debugElement.query(
      By.css('.card__value')
    ).nativeElement;
    expect(cardValue.textContent).toContain('35');
  });
});

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-calculator-section',
  templateUrl: './calculator-section.component.html',
  styleUrls: ['./calculator-section.component.scss'],
})
export class CalculatorSectionComponent {
  @Input() w100?: boolean;
}

import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { CalculatorComponent } from './calculator.component';
import { CalculatorService } from 'src/app/services/calculator.service';
import { CalculatorState } from 'src/app/utils/calculator-state.enum';
import { of } from 'rxjs';
import { CalculatorSectionComponent } from '../calculator-section/calculator-section.component';
import { AmountInputComponent } from '../amount-input/amount-input.component';

describe('CalculatorComponent', () => {
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;
  let mockCalculatorService: jasmine.SpyObj<CalculatorService>;

  beforeEach(() => {
    mockCalculatorService = jasmine.createSpyObj('CalculatorService', [
      'getCombination',
    ]);

    TestBed.configureTestingModule({
      declarations: [
        CalculatorComponent,
        CalculatorSectionComponent,
        AmountInputComponent,
      ],
      providers: [
        { provide: CalculatorService, useValue: mockCalculatorService },
      ],
    });

    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize component', () => {
    fixture.detectChanges();
    expect(component.previousSuggestion).toBeUndefined();
    expect(component.nextSuggestion).toBeUndefined();
    expect(component.state).toBeUndefined();
    expect(component.amountUpdated).toBeFalse();
    expect(component.disabled).toBeFalse();
    expect(component.isValidateDisabled).toBeTrue();
  });

  it('should enable validation button when desiredAmount is updated', () => {
    component.value.value = 70;
    component.amountUpdated = true;
    fixture.detectChanges();
    expect(component.isValidateDisabled).toBeFalse();
  });

  it('should process desired amount and update state and value', fakeAsync(() => {
    const mockCombination = { equal: { value: 70, cards: [35, 35] } };
    mockCalculatorService.getCombination.and.returnValue(of(mockCombination));

    component.value.value = 70;
    component.amountUpdated = true;
    component.processDesiredAmount();
    tick();

    expect(component.state).toBe(CalculatorState.equal);
    expect(component.value).toEqual(mockCombination.equal);
    expect(component.amountUpdated).toBeFalse();
  }));

  it('should set suggestion as desired amount', () => {
    const mockSuggestion = { value: 51, cards: [26, 25] };
    component.setSuggestionAsDesiredAmount(mockSuggestion);

    expect(component.value).toEqual(mockSuggestion);
    expect(component.previousSuggestion).toBeUndefined();
    expect(component.nextSuggestion).toBeUndefined();
    expect(component.state).toBe(CalculatorState.equal);
    expect(component.amountUpdated).toBeFalse();
  });

  it('should write value', () => {
    const mockValue = { value: 20, cards: [20] };
    component.writeValue(mockValue);

    expect(component.value).toEqual(mockValue);
  });

  it('should disable validation button when desiredAmount is not updated', () => {
    component.value.value = 20;
    component.amountUpdated = false;
    fixture.detectChanges();
    expect(component.isValidateDisabled).toBeTrue();
  });

  it('should process desired amount and handle too low suggestion', fakeAsync(() => {
    const mockCombination = { ceil: { value: 20, cards: [20] } };
    mockCalculatorService.getCombination.and.returnValue(of(mockCombination));

    component.value.value = 1;
    component.amountUpdated = true;
    component.processDesiredAmount();
    tick();

    expect(component.state).toBe(CalculatorState.tooLow);
    expect(component.value).toEqual(mockCombination.ceil);
    expect(component.amountUpdated).toBeFalse();
  }));

  it('should process desired amount and handle too high suggestion', fakeAsync(() => {
    const mockCombination = { floor: { value: 70, cards: [35, 35] } };
    mockCalculatorService.getCombination.and.returnValue(of(mockCombination));

    component.value.value = 100;
    component.amountUpdated = true;
    component.processDesiredAmount();
    tick();

    expect(component.state).toBe(CalculatorState.tooHigh);
    expect(component.value).toEqual(mockCombination.floor);
    expect(component.amountUpdated).toBeFalse();
  }));

  it('should process desired amount and handle no possible combination suggestion', fakeAsync(() => {
    const mockCombination = {
      floor: { value: 51, cards: [26, 25] },
      ceil: { value: 55, cards: [35, 20] },
    };
    mockCalculatorService.getCombination.and.returnValue(of(mockCombination));

    component.value.value = 52;
    component.amountUpdated = true;
    component.processDesiredAmount();
    tick();

    expect(component.state).toBe(CalculatorState.none);
    expect(component.value).toEqual({ value: 52, cards: [] });
    expect(component.previousSuggestion).toEqual(mockCombination.floor);
    expect(component.nextSuggestion).toEqual(mockCombination.ceil);
    expect(component.amountUpdated).toBeFalse();
  }));

  it('should process desired amount and handle no possible combination suggestion with undefined floor/ceil', fakeAsync(() => {
    const mockCombination = {};
    mockCalculatorService.getCombination.and.returnValue(of(mockCombination));

    component.value.value = 20;
    component.amountUpdated = true;
    component.processDesiredAmount();
    tick();

    expect(component.state).toBe(CalculatorState.none);
    expect(component.value).toEqual({ value: 20, cards: [] });
    expect(component.previousSuggestion).toBeUndefined();
    expect(component.nextSuggestion).toBeUndefined();
    expect(component.amountUpdated).toBeFalse();
  }));

  it('should disable validation button when form is disabled', () => {
    component.disabled = true;
    component.value.value = 20;
    component.amountUpdated = true;
    fixture.detectChanges();
    expect(component.isValidateDisabled).toBeTrue();
  });

  it('should set disabled state', () => {
    component.setDisabledState(true);
    expect(component.disabled).toBeTrue();
  });
});

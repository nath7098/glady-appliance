import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CalculatorComponentValue } from 'src/app/models/calculator-component-value.model';
import { CalculatorService } from 'src/app/services/calculator.service';
import { CalculatorState } from 'src/app/utils/calculator-state.enum';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CalculatorComponent),
      multi: true,
    },
  ],
})
export class CalculatorComponent implements OnInit, ControlValueAccessor {
  @Input() title?: string;

  previousSuggestion?: CalculatorComponentValue;
  nextSuggestion?: CalculatorComponentValue;
  state?: CalculatorState;
  amountUpdated: boolean = false;
  disabled: boolean = false;

  value: CalculatorComponentValue = {} as CalculatorComponentValue;

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(private calculatorService: CalculatorService) {}

  ngOnInit(): void {}

  get isValidateDisabled() {
    return !this?.value?.value || !this.amountUpdated || this.disabled;
  }

  /**
   * Calling the api to retrieve the possible cards combination to display to the customer
   *
   */
  processDesiredAmount() {
    if (!this.isValidateDisabled) {
      this.calculatorService
        .getCombination(5, this?.value?.value!)
        .subscribe((result) => {
          this.amountUpdated = false;
          if (this.value) {
            this.value.cards = [];
          }
          this.previousSuggestion = undefined;
          this.nextSuggestion = undefined;
          if (result.equal) {
            // the asked amount is possible
            this.state = CalculatorState.equal;
            this.value = result.equal;
            this.onChange(result.equal);
          } else if (result.ceil && !result.floor) {
            // the asked amount is too low
            this.state = CalculatorState.tooLow;
            this.value = result.ceil;
            // this.desiredAmount = result.ceil.value;
            this.onChange(result.ceil);
          } else if (result.floor && !result.ceil) {
            // the asked amount is too high
            this.state = CalculatorState.tooHigh;
            this.value = result.floor;
            // this.desiredAmount = result.floor.value;
            this.onChange(result.floor);
          } else {
            // the asked amount is not possible but let's make a suggestion
            this.state = CalculatorState.none;
            this.previousSuggestion = result.floor;
            this.nextSuggestion = result.ceil;
            if (this.value) {
              this.value.cards = [];
            }
            this.onChange(undefined);
          }
        });
    }
  }

  /**
   * Setting wether the input has been updated
   *
   * @param value
   */
  setAmountUpdated(value: boolean = true) {
    this.amountUpdated = value;
  }

  /**
   * Setting a suggestion selected by the customer as the desired amount
   *
   * @param suggestion : The selected suggestion
   */
  setSuggestionAsDesiredAmount(suggestion: CalculatorComponentValue) {
    // this.desiredAmount = suggestion.value;
    this.value = suggestion;
    this.previousSuggestion = undefined;
    this.nextSuggestion = undefined;
    this.state = CalculatorState.equal;
    this.setAmountUpdated(false);

    this.onChange(suggestion);
  }

  writeValue(value: CalculatorComponentValue): void {
    this.value = value;
    if (value?.value) {
      this.state = CalculatorState.equal;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}

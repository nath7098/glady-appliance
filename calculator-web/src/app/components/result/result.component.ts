import { Component, Input } from '@angular/core';
import { CalculatorState } from 'src/app/utils/calculator-state.enum';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})
export class ResultComponent {
  @Input() cards?: number[];
  @Input() state?: CalculatorState;

  get isStateEqual() {
    return this.state === CalculatorState.equal;
  }
  get isStateTooLow() {
    return this.state === CalculatorState.tooLow;
  }
  get isStateTooHigh() {
    return this.state === CalculatorState.tooHigh;
  }
  get isStateNone() {
    return this.state === CalculatorState.none;
  }
}

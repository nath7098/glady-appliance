import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { CalculatorSectionComponent } from '../calculator-section/calculator-section.component';
import { ResultComponent } from './result.component';
import { CardComponent } from '../card/card.component';
import { CalculatorState } from 'src/app/utils/calculator-state.enum';

describe('CalculatorSectionComponent', () => {
  let component: ResultComponent;
  let fixture: ComponentFixture<ResultComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ResultComponent, CardComponent],
    });

    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have isStateEqual true when state is equal', () => {
    component.state = CalculatorState.equal;
    fixture.detectChanges();

    expect(component.isStateEqual).toBeTrue();
  });

  it('should have isStateEqual false when state is another', () => {
    component.state = CalculatorState.tooHigh;
    fixture.detectChanges();

    expect(component.isStateEqual).toBeFalse();
  });
});

export enum CalculatorState {
  equal = 0,
  tooLow = 1,
  tooHigh = 2,
  none = 3,
}

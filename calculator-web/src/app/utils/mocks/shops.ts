import { Shop } from 'src/app/models/shop.model';

const shops: Shop[] = [{ id: 5, name: 'WedooStore' } as Shop];

export const findShop = (id: number): Shop | undefined => {
  return shops.find((shop) => shop.id === id);
};

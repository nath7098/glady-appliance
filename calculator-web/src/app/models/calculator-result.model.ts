import { CalculatorState } from '../utils/calculator-state.enum';
import { CalculatorComponentValue } from './calculator-component-value.model';

export interface calculatorResult {
  equal?: CalculatorComponentValue;
  floor?: CalculatorComponentValue;
  ceil?: CalculatorComponentValue;
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { calculatorResult } from '../models/calculator-result.model';

@Injectable({
  providedIn: 'root',
})
export class CalculatorService {
  private BASE_URL = 'http://localhost:3000';
  private AUTH_TOKEN = 'tokenTest123';

  constructor(private httpClient: HttpClient) {}

  getCombination(
    shopId: number,
    desiredAmount: number
  ): Observable<calculatorResult> {
    return this.httpClient.get<calculatorResult>(
      `${this.BASE_URL}/shop/${shopId}/search-combination?amount=${desiredAmount}`,
      { headers: { Authorization: this.AUTH_TOKEN } }
    );
  }
}

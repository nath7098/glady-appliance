import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { CalculatorService } from './calculator.service';
import { calculatorResult } from '../models/calculator-result.model';

describe('CalculatorService', () => {
  let calculatorService: CalculatorService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CalculatorService],
    });

    calculatorService = TestBed.inject(CalculatorService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(calculatorService).toBeTruthy();
  });

  it('should retrieve combination from the API', () => {
    const mockShopId = 5;
    const mockDesiredAmount = 70;

    const mockResponse: calculatorResult = {
      equal: { value: 70, cards: [35, 35] },
      floor: { value: 70, cards: [35, 35] },
      ceil: { value: 70, cards: [35, 35] },
    };

    calculatorService
      .getCombination(mockShopId, mockDesiredAmount)
      .subscribe((response) => {
        expect(response).toEqual(mockResponse);
      });

    const req = httpTestingController.expectOne(
      `http://localhost:3000/shop/${mockShopId}/search-combination?amount=${mockDesiredAmount}`
    );

    expect(req.request.method).toEqual('GET');
    expect(req.request.headers.get('Authorization')).toEqual('tokenTest123');

    req.flush(mockResponse);
  });
});
